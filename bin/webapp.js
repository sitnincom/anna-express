#!/usr/bin/env node

"use strict";

const path = require("path");
const fs = require("fs");
const ACore = require("anna/ACore");

const pkgInfo = require(path.join(process.cwd(), "package.json"));

process.title = pkgInfo.name+" webapp";

class Application extends ACore {
    main () {
        this.log.debug(`${pkgInfo.name} application starts...`);
        this.modules.get("anna-express").listen().then(info => {
            this.log.info(`Listening at http://${info.host}:${info.port}`);
        }).catch(err => {
            this.handleFatalError(new Error(`Failed to listen: ${err.message}`));
        });
    }
}

const configDir = !!process.argv[2] ? process.argv[2] : "./etc";
let configDirStats = null;

try {
    configDirStats = fs.statSync(configDir);
} catch (err) {};

if (!!configDirStats && configDirStats.isDirectory()) {
    const app = new Application(configDir, pkgInfo.version);
    app.run();
} else {
    console.error(`Cannot use [${configDir}] as a config file directory`);
    process.exit(1);
}
