"use strict";

const Promise = require("bluebird");
const express = require("express");
const AModule = require("anna/AModule");

module.exports = class AWebappModule extends AModule {
    constructor (core, settings) {
        super(core, settings);
        this.requiredModules = ["anna-express"];

        this._router = null;
        this._urls = new Set();
    }

    get express () {
        return this.modules.get("anna-express").app;
    }

    get router () {
        if (!this._router) {
            this._router = express.Router({ strict: true, caseSensitive: true });
        }
        return this._router;
    }

    get nunjucks () {
        return this.modules.get("anna-express").nunjucks;
    }

    injectLocals (updater) {
        this.modules.get("anna-express").injectLocals(updater);
    }

    init () {
        this._urls.forEach((urlDefs) => {
            if (4 === urlDefs.length) {
                this.log.debug(`Mounting ${urlDefs[0]} ${urlDefs[1]} with middlewares`);
                this.router[urlDefs[0]](urlDefs[1], urlDefs[3], urlDefs[2].bind(this));
            } else {
                this.log.debug(`Mounting ${urlDefs[0]} ${urlDefs[1]} alone`);
                this.router[urlDefs[0]](urlDefs[1], urlDefs[2].bind(this));
            }
        });

        return Promise.resolve();
    }

    postInit () {
        this.log.debug(`${this.constructor.name}.postInit()`);
        super.postInit();

        this.express.use(this.settings.mountPoint, this.router);
    }
}
