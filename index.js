"use strict";

const Promise = require("bluebird");
const AModule = require("anna/AModule");
const express = require("express");
const path = require("path");
const nunjucks = require("nunjucks");
const moment = require("moment");
const marked = require("marked");

marked.setOptions({
    renderer: new marked.Renderer(),
    gfm: true,
    tables: true,
    breaks: false,
    pedantic: false,
    sanitize: true,
    smartLists: true,
    smartypants: false
}); 

module.exports = class ExpressModule extends AModule {
    constructor (core, settings) {
        super(core, settings);

        this._localsUpdaters = new Set();
    }

    get app () {
        return this._express;
    }

    get nunjucks () {
        if (!this._nunjucks) {
            this._nunjucks = new nunjucks.Environment(new nunjucks.FileSystemLoader(this.settings.templates_dir, { watch: true }));
        }

        return this._nunjucks;
    }
    
    get marked () {
        return marked;
    }

    _initExpress () {
        this._express = express();
        this._httpServer = require("http").Server(this._express);

        this.app.set("trust proxy", true);
        this.app.set("views", this.settings.templates_dir);

        this.app.use(require('jumanji'));

        if (this.core.debug && this.settings.debugPanel) {
            require('express-debug')(this.app);
        }
    }

    _initNunjucks () {
        this.log.debug(`Nunjucks templates in "${this.settings.templates_dir}"`);

        this.nunjucks.express(this.app);

        function _filter_formatDate (date, format, locale, tz) {
            if (!date) return date;

            var m = moment(date);
            m.locale(locale || "en");
            if (tz) {
                m.utcOffset(tz);
            }

            return m.format(format);
        }

        function _filter_marked (content) {
            return marked(content);
        }


        this.nunjucks.addFilter('date', _filter_formatDate);
        this.nunjucks.addFilter('md', _filter_marked);
    }

    injectRouter (root, router) {
        this.app.use(root, router);
    }

    injectMiddleware (middleware) {
        this.app.use(middleware);
    }

    injectLocals (updater) {
        this._localsUpdaters.add(updater);
    }

    listen () {
        return new Promise((resolve, reject) => {
            const port = this._settings.port;
            const host = this._settings.host || "127.0.0.1";

            this._httpServer.once("error", err => {
                reject(err);
            });

            this._httpServer.listen(port, host, _ => {
                resolve({host: host, port: port});
            });
            
            return null;
        });
    }

    updateLocalGlobals (req, res) {
        res.locals.debug = this.core.debug;
        res.locals.appVersion = this._core.projectVersion;
        res.locals.settings = this.settings;
        res.locals.site = this.core._config.get("global");
        res.locals.site.now = new Date();
        res.locals.domain = this.core._config.get("global.domain");
        res.locals.url = req._parsedOriginalUrl.pathname;
    }

    _updateLocals (req, res, next) {
        this.log.debug(`${this.constructor.name}._updateLocals()`);
        
        const queue = [];
        this._localsUpdaters.forEach(updater => {
            queue.push(updater(req, res));
        });
        
        Promise.all(queue).then(_ => {
            next();
        }).catch(err => {
            next(err);
        });
    }

    init () {
        this.log.debug(`${this.constructor.name}.init()`);
        return new Promise((resolve, reject) => {
            try {
                this._initExpress();
                this._initNunjucks();
                this._localsUpdaters.add(this.updateLocalGlobals.bind(this));
            } catch (err) {
                reject(err);
            }
            resolve();
        });
    }

    postInit () {
        this.log.debug(`${this.constructor.name}.postInit()`);
        super.postInit();
        this.app.use(this._updateLocals.bind(this));
    }
}
